//package com.example.springbootdemo.car;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.List;
//
//@Configuration
//public class CarConfig {
//    @Bean
//    CommandLineRunner commandLineRunner(CarRepository repository){
//        return args -> {
//           Car mitsubishi = new Car(
//              "Mitsubishi",
//              "L300 FB Deluxe",
//              "Japan Technology",
//              "2016"
//            );
//
//           Car kawasaki  = new Car(
//                    "Kawasaki",
//                    "Fury 155",
//                    "Japan Technology",
//                    "2007"
//           );
//
//           repository.saveAll(
//                   List.of(mitsubishi, kawasaki)
//           );
//        };
//    };
//}
