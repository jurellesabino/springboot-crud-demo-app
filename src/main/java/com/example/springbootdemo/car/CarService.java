package com.example.springbootdemo.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getCars() {
        return carRepository.findAll();
    }
    public Car getCarById(Integer carId) {
//        boolean exists = carRepository.existsById(carId);
//        if(!exists) {
//            throw new IllegalStateException("Car not found.");
//        }
        return carRepository.findById(carId).get();
    }

    public void addNewCar(Car car) {
        carRepository.save(car);
    }

    public void deleteCar(Integer carId) {
       boolean exists = carRepository.existsById(carId);
        if(!exists) {
            throw new IllegalStateException("Car for Deletion not found.");
        }
        carRepository.deleteById(carId);
    }

    @Transactional
    public Car updateCar(Integer carId, Car car) {
         Car existing= carRepository.findById(carId).orElseThrow(
                ()-> new IllegalStateException("Car not found.")
        );
         existing.setMake(car.getMake());
         existing.setModel(car.getModel());
         existing.setBrand(car.getBrand());
         existing.setYear(car.getYear());
         existing = carRepository.save(existing);
         return existing;
    }
}
