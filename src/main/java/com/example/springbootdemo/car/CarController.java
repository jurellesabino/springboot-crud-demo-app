package com.example.springbootdemo.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/cars")
public class CarController {
    @Autowired
    private CarService carService;

    @GetMapping
    public List<Car> getCars() {
        return carService.getCars();
    }

    @GetMapping(path="{carId}")
    public Car getCars(@PathVariable("carId") Integer carId) {
        return carService.getCarById(carId);
    }
    @PostMapping
    public void registerNewCar(@RequestBody Car car) {
        carService.addNewCar(car);
    }

    @DeleteMapping(path="{carId}")
    public void deleteCar(@PathVariable("carId") Integer carId) {
        carService.deleteCar(carId);
    }

    @PutMapping(path="{carId}")
    public Car updateCar(
        @PathVariable("carId") Integer carId,
      @RequestBody Car car) {
       return carService.updateCar(carId, car);
    }
}
