package com.example.springbootdemo.car;

import javax.persistence.*;

@Entity
@Table
public class Car {
    @SequenceGenerator(
            name="car_sequence",
            sequenceName="car_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy=GenerationType.SEQUENCE,
            generator = "car_sequence"
    )
    @Id
    private Integer id;

    @Column(name="brand")
    private String brand;

    @Column(name="model")
    private String model;

    @Column(name="make")
    private String make;

    @Column(name="year")
    private String year;

    public Car() {
    }

    public Car(Integer id, String brand, String model, String make, String year) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.make = make;
        this.year = year;
    }

    public Car(String brand, String model, String make, String year) {
        this.brand = brand;
        this.model = model;
        this.make = make;
        this.year = year;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", make='" + make + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
